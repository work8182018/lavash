<section>
    <div class="assortiment">
        <div class="assortiment_text">
            <h2>Асортимент</h2>
            <p class="text">Перевагою пекарень «Ваш Лаваш» є відкрита кухня, де кожен може спостерігати весь процес приготування випічки. Кожна пекарня – особлива й неповторна.
                Це як маленьке виробництво, оскільки всі пекарні повного циклу – від замішування тіста до продажу готової продукції.</p>
            <p class="assortiment_text_opacity">Натисніть на кнопку «Переглянути все»,
                щоб побачити більше позицій та фото →</p>
        </div>
        <div class="assortiment_img">
            <img src="{{asset('storage/images/assortiment/img1.png')}}" alt="">
            <div class="menu">
                <h4 class="menu_header">Солодка випічка:</h4>
                <ul class="menu_list">
                    <li>Наполеоні</li>
                    <li>Андрути</li>
                    <li>Слойки</li>
                    <li>Када з маком</li>
                </ul>
                <a class="menu_link" href="#" @click="$dispatch('modal-1')" onclick="return false">Переглянути все</a>
              </div>
        </div>
        <div class="assortiment_slider">
            <div class="swiper" >
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide"><div class="img"><img src="{{asset('storage/images/assortiment/img2.png')}}" alt=""></div><h4>Солона випічка</h4></div>
                    <div class="swiper-slide"><div class="img"><img src="{{asset('storage/images/assortiment/img3.png')}}" alt=""></div><h4>Солодка випічка</h4></div>
                    <div class="swiper-slide"><div class="img"><img src="{{asset('storage/images/assortiment/img4.png')}}" alt=""></div><h4>Паляниця</h4></div>
                    <div class="swiper-slide"><div class="img"><img src="{{asset('storage/images/assortiment/img2.png')}}" alt=""></div><h4>Солона випічка</h4></div>
                    <div class="swiper-slide"><div class="img"><img src="{{asset('storage/images/assortiment/img3.png')}}" alt=""></div><h4>Солодка випічка</h4></div>
                    <div class="swiper-slide"><div class="img"><img src="{{asset('storage/images/assortiment/img4.png')}}" alt=""></div><h4>Паляниця</h4></div>
                </div>
                <!-- If we need navigation buttons -->
            </div>
            <div class="swiper-pagination">
                {{--<div class="swiper-pagination-current"></div>--}}
                {{--<div class="swiper-pagination-total"></div>--}}
            </div>
            <div class="swiper-prev">
                <svg xmlns="http://www.w3.org/2000/svg" width="" height="" viewBox="0 0 103 104" fill="none">
                    <g filter="url(#filter0_d_740_652)">
                        <ellipse cx="51.5001" cy="47.9473" rx="32" ry="32.5" fill="#F0F0F0"/>
                    </g>
                    <path d="M52.5363 37.1396L42.6368 47.0391L52.5363 56.9386" stroke="#2F2F2F" stroke-width="3" stroke-linecap="round"/>
                    <defs>
                        <filter id="filter0_d_740_652" x="0.500122" y="0.447266" width="102" height="103" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                            <feMorphology radius="1" operator="dilate" in="SourceAlpha" result="effect1_dropShadow_740_652"/>
                            <feOffset dy="4"/>
                            <feGaussianBlur stdDeviation="9"/>
                            <feComposite in2="hardAlpha" operator="out"/>
                            <feColorMatrix type="matrix" values="0 0 0 0 0.5625 0 0 0 0 0.5625 0 0 0 0 0.5625 0 0 0 0.25 0"/>
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_740_652"/>
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_740_652" result="shape"/>
                        </filter>
                    </defs>
                </svg>
            </div>
            <div class="swiper-next">
                <svg xmlns="http://www.w3.org/2000/svg" width="" height="" viewBox="0 0 103 103" fill="none">
                    <g filter="url(#filter0_d_740_655)">
                        <ellipse cx="51.4999" cy="47.5" rx="32" ry="32.5" fill="#F0F0F0"/>
                    </g>
                    <path d="M49.8152 56.5059L59.7147 46.6064L49.8152 36.7069" stroke="#2F2F2F" stroke-width="3" stroke-linecap="round"/>
                    <defs>
                        <filter id="filter0_d_740_655" x="0.499878" y="0" width="102" height="103" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                            <feMorphology radius="1" operator="dilate" in="SourceAlpha" result="effect1_dropShadow_740_655"/>
                            <feOffset dy="4"/>
                            <feGaussianBlur stdDeviation="9"/>
                            <feComposite in2="hardAlpha" operator="out"/>
                            <feColorMatrix type="matrix" values="0 0 0 0 0.5625 0 0 0 0 0.5625 0 0 0 0 0.5625 0 0 0 0.25 0"/>
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_740_655"/>
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_740_655" result="shape"/>
                        </filter>
                    </defs>
                </svg>
            </div>
        </div>
    </div>
</section>