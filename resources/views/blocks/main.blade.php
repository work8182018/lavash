<section class="main-block">
    <div class="main-block__buns">
        <img class="main-block__bun main-block__top-left-bun" src="{{ Storage::url('images/main-block_bun-tl.webp') }}" alt="bun">
        <img class="main-block__bun main-block__top-bun" src="{{ Storage::url('images/main-block_bun-t.webp') }}" alt="bun">
        <img class="main-block__bun main-block__top-right-bun" src="{{ Storage::url('images/main-block_bun-tr.webp') }}" alt="bun">
        <img class="main-block__bun main-block__bottom-left-bun" src="{{ Storage::url('images/main-block_bun-bl.webp') }}" alt="bun">
        <img class="main-block__bun main-block__bottom-bun" src="{{ Storage::url('images/main-block_bun-b.webp') }}" alt="bun">
        <img class="main-block__bun main-block__bottom-right-bun" src="{{ Storage::url('images/main-block_bun-br.webp') }}" alt="bun">
    </div>
    <div class="main-block__wrapper">
        <div class="main-block__content">
            <h1 class="main-block__title">Пекарня Ваш Лаваш</h1>
            <p class="main-block__text">Для улюблених — все найкраще</p>
            <x-button>Відкрити пекарню</x-button>
        </div>
    </div>
</section>
