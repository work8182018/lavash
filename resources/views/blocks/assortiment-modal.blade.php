<div x-data="{ modal: false }">
        <div class="assortiment_modal modal-wrapper" :class="{ 'active': modal }" x-on:modal-1.window="modal = !modal">
            <div class="backdrop backdrop-shaded" x-show.transition.opacity.duration.600ms="modal" @click="modal = false" style="display: none;"></div>
            <div class="modal-panel" :class="{ 'active': modal }">
                <div class="assortiment_modal_slider">
                    <div class="swiper">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide"><img src="{{asset('storage/images/assortiment/img5.jpg')}}" alt=""></div>
                            <div class="swiper-slide"><img src="{{asset('storage/images/assortiment/img6.jpg')}}" alt=""></div>
                            <div class="swiper-slide"><img src="{{asset('storage/images/assortiment/img7.jpg')}}" alt=""></div>
                            <div class="swiper-slide"><img src="{{asset('storage/images/assortiment/img8.jpg')}}" alt=""></div>
                            <div class="swiper-slide"><img src="{{asset('storage/images/assortiment/img9.jpg')}}" alt=""></div>
                            <div class="swiper-slide"><img src="{{asset('storage/images/assortiment/img10.jpg')}}" alt=""></div>
                        </div>
                    </div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-prev">
                        <svg xmlns="http://www.w3.org/2000/svg" width="" height="" viewBox="0 0 103 104" fill="none">
                            <g filter="url(#filter0_d_740_652)">
                                <ellipse cx="51.5001" cy="47.9473" rx="32" ry="32.5" fill="#F0F0F0"/>
                            </g>
                            <path d="M52.5363 37.1396L42.6368 47.0391L52.5363 56.9386" stroke="#2F2F2F" stroke-width="3" stroke-linecap="round"/>
                            <defs>
                                <filter id="filter0_d_740_652" x="0.500122" y="0.447266" width="" height="" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                    <feMorphology radius="1" operator="dilate" in="SourceAlpha" result="effect1_dropShadow_740_652"/>
                                    <feOffset dy="4"/>
                                    <feGaussianBlur stdDeviation="9"/>
                                    <feComposite in2="hardAlpha" operator="out"/>
                                    <feColorMatrix type="matrix" values="0 0 0 0 0.5625 0 0 0 0 0.5625 0 0 0 0 0.5625 0 0 0 0.25 0"/>
                                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_740_652"/>
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_740_652" result="shape"/>
                                </filter>
                            </defs>
                        </svg>
                    </div>
                    <div class="swiper-next">
                        <svg xmlns="http://www.w3.org/2000/svg" width="" height="" viewBox="0 0 102 103" fill="none">
                            <g filter="url(#filter0_d_668_293)">
                                <ellipse cx="51" cy="47.5" rx="32" ry="32.5" fill="#F0F0F0"/>
                            </g>
                            <path d="M49.3153 56.5059L59.2148 46.6064L49.3153 36.7069" stroke="#2F2F2F" stroke-width="3" stroke-linecap="round"/>
                            <defs>
                                <filter id="filter0_d_668_293" x="0" y="0" width="102" height="103" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                    <feMorphology radius="1" operator="dilate" in="SourceAlpha" result="effect1_dropShadow_668_293"/>
                                    <feOffset dy="4"/>
                                    <feGaussianBlur stdDeviation="9"/>
                                    <feComposite in2="hardAlpha" operator="out"/>
                                    <feColorMatrix type="matrix" values="0 0 0 0 0.5625 0 0 0 0 0.5625 0 0 0 0 0.5625 0 0 0 0.25 0"/>
                                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_668_293"/>
                                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_668_293" result="shape"/>
                                </filter>
                            </defs>
                        </svg>
                    </div>
                </div>
                <div class="assortiment_modal_text">
                    <h2>Солона випічка:</h2>
                    <div class="list">
                        <h4 class="name">Самса</h4>
                        <p class="description">ніжне слоєне тісто, мелена телятина з овочами </p>
                        <h4 class="name">Лорі пурі </h4>
                        <p class="description">багато сиру та соковита шинка</p>
                        <h4 class="name">Гурулі</h4>
                        <p class="description">багато сиру та гриби</p>
                        <h4 class="name">Катамі пурі</h4>
                        <p class="description">ніжний шпинат та м’ясо курчати, сир</p>
                        <h4 class="name">Люля кебаб</h4>
                        <p class="description">мелена свинина та овочі</p>
                        <h4 class="name">Три м’яса</h4>
                        <p class="description">ніжне слоєне тісто, мелена телятина з овочами </p>
                        <h4 class="name">Самса</h4>
                        <p class="description">ніжне слоєне тісто, мелена телятина з овочами </p>
                        <h4 class="name">Самса</h4>
                        <p class="description">ніжне слоєне тісто, мелена телятина з овочами </p>
                        <h4 class="name">Самса</h4>
                        <p class="description">ніжне слоєне тісто, мелена телятина з овочами </p>
                    </div>
                </div>
                <a class="assortiment_modal_close" @click="modal = false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="" height="" viewBox="0 0 57 57" fill="none">
                        <path d="M43.1343 14.8491L14.85 43.1334" stroke="#2F2F2F" stroke-width="2" stroke-linecap="round"/>
                        <path d="M41.7202 43.1335L13.4359 14.8493" stroke="#2F2F2F" stroke-width="2" stroke-linecap="round"/>
                    </svg>
                </a>
            </div>
        </div>
</div>
