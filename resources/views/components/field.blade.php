<label class="field {{ $attributes->get('class') }}"
       x-data="{ id: `input${(new Date()).getTime()}.${Math.random()}`, valid: true }"
       x-bind:class="!valid ? 'field--invalid' : ''"
       :for="id">
    <p class="field__label">
        {{ $slot }}
        @if($attributes->get('required'))
            <span class="text-red">*</span>
        @endif
    </p>
    <input x-on:input="valid = true" x-on:invalid="valid = false"
           {{ $attributes->merge(['class' => 'field__input', ':id' => 'id' ]) }}
           @required($attributes->get('required')) />
</label>
