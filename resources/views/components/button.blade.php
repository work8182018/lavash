<button {{ $attributes->class(['button', 'button--white' => $attributes->get('white')]) }}>
    {{ $slot }}
</button>
