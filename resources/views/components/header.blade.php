<header class="header container">
    <x-logo class="header__logo"/>
    <div class="header__navigation">
        <a href="#" class="header__link">Про нас</a>
        <a href="#" class="header__link">Асортимент</a>
        <a href="#" class="header__link">Локації</a>
        <a href="#" class="header__link">Новини</a>
        <a href="#" class="header__link">Франшиза</a>
        <a href="#" class="header__link">Контакти</a>
    </div>
    <div class="header__social-list">
        <x-social-button class="header__social-link" href="/" icon="linkedin"/>
        <x-social-button class="header__social-link" href="/" icon="facebook"/>
        <x-social-button class="header__social-link" href="/" icon="tiktok"/>
        <x-social-button class="header__social-link" href="/" icon="youtube"/>
    </div>
    <x-button class="header__button">Стати інвестором</x-button>
</header>
