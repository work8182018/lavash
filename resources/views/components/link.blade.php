<a {{ $attributes->class(['link']) }}>
    {{ $slot }}
</a>
