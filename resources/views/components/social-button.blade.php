<div {{ $attributes->class(['social-button', 'social-button--white' => $white]) }}>
    {!! file_get_contents($icon) !!}
</div>
