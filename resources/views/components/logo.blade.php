@if(Route::current()->getName() === 'home')
    <img {{ $attributes->class(['logo']) }}
         src="{{ Storage::url('images/logo.webp') }}"
         alt="Логотип">
@else
    <a {{ $attributes->class(['logo']) }} href="{{ route('home') }}">
        <img src="{{ Storage::url('images/logo.webp') }}" alt="Логотип">
    </a>
@endif
