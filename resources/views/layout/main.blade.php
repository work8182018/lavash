<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        {{-- Favicon --}}
        <link rel="shortcut icon" href="{{ Storage::url('icons/dumpling-yellow.svg') }}" type="image/svg">

        {{-- Fonts --}}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fontsource/comfortaa@5.0.5/cyrillic-400.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fontsource/comfortaa@5.0.5/cyrillic-500.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fontsource/comfortaa@5.0.5/cyrillic-700.min.css">

        {{-- Normalize.css --}}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.min.css">

        {{-- Alpine.js --}}
        <script src="https://cdn.jsdelivr.net/npm/alpinejs@3.12.3/dist/cdn.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@alpinejs/mask@3.12.3/dist/cdn.min.js"></script>

        {{-- Swiper --}}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10.1.0/swiper-bundle.min.css">
        <script src="https://cdn.jsdelivr.net/npm/swiper@10.1.0/swiper-bundle.min.js"></script>

        <title>
            @hasSection('title')
                @yield('title') |
            @endif
            {{ env('APP_NAME') }}
        </title>

        @vite(['resources/js/app.js', 'resources/css/app.styl','resources/scss/style.scss'])
    </head>
    <body>
        <div class="main">
            <x-header />
            <div class="main__content">
                @yield('content')
            </div>
        </div>
    </body>
    <script>
        let appendNumber = 6;
        let prependNumber = 1;
        const swiper = new Swiper('.assortiment_slider >.swiper', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            slidesPerView: 3,
            spaceBetween: 10,
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },
            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                375: {
                    slidesPerView: 3,
                    spaceBetween: 5
                },
                // when window width is >= 640px
                500: {
                    slidesPerView: 3,
                    spaceBetween: 5
                }
            },
            // Navigation arrows
            navigation: {
                nextEl: '.assortiment_slider >.swiper-next',
                prevEl: '.assortiment_slider >.swiper-prev',
            },
        });
        const swiperModal = new Swiper('.assortiment_modal .swiper', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            slidesPerView: 1,
            spaceBetween: 10,
            // Navigation arrows
            navigation: {
                nextEl: '.assortiment_modal .swiper-next',
                prevEl: '.assortiment_modal .swiper-prev',
            },
        });
        // блокировка прокрутки при открытии модалки alpine.js
        document.addEventListener('click', () => {
            const scrLock = Array.from(document.querySelectorAll('.modal-wrapper.active'))
            if (scrLock.length) {
                document.querySelector('body').classList.add('body-scroll-lock')
            }
            else {
                document.querySelector('body').classList.remove('body-scroll-lock')
            }
        });

    </script>
</html>
