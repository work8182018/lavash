@extends('layout.main')

@section('title', 'Головна')

@section('content')
    @include('blocks.main')
    @include('blocks.assortiment')
    @include('blocks.assortiment-modal')

@endsection
