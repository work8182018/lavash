import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

// export default defineConfig({
//     plugins: [
//         laravel({
//             input: ['resources/css/app.styl','resources/scss/style.scss', 'resources/js/app.js'],
//             refresh: true,
//         }),
//     ],
//     build: {
//     assetsDir: '',
// }
// });

var config = {
    plugins: [
        laravel({
          input: ['resources/css/app.styl','resources/scss/style.scss', 'resources/js/app.js'],
           refresh: true,
       }),
    ],
    build: {
        assetsDir: '',
    }
}

export default defineConfig(({command, mode, ssrBuild}) => {
    if (command === 'serve') {
        config.publicDir = 'public';
        config.build = {
            assetsDir: '',
            copyPublicDir: false,
            emptyOutDir: true,
        };
    }
    return config;
});