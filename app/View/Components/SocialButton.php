<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\Component;

class SocialButton extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $icon,
        public bool $white = false,
    )
    {
        $this->icon = 'public/icons/'.$this->icon.'.svg';
        if (!Storage::exists($this->icon)) {
            throw new \Exception('Icon '.$this->icon.' not found.');
        }
        $this->icon = Storage::path($this->icon);
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.social-button');
    }
}
